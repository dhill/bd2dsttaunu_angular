import os, sys
os.environ["ROOT_INCLUDE_PATH"] = os.pathsep + "/home/$USER/bd2dsttaunu_angular/Meerkat/inc/"

from ROOT import gSystem, gStyle, RooRealVar, gApplication, TChain, AddressOf

gSystem.Load("/home/$USER/bd2dsttaunu_angular/Meerkat/lib/libMeerkat.so")

from ROOT import OneDimPhaseSpace, FormulaDensity, CombinedPhaseSpace
from ROOT import Logger
from ROOT import BinnedKernelDensity, BinnedDensity, AdaptiveKernelDensity
from ROOT import TFile, TNtuple, TCanvas, TH1F, TH2F, TH3F, TText, vector
from ROOT import RooFit, RooDataSet, RooDataHist, RooHistPdf, RooRealVar, RooFormulaVar, RooArgSet, RooArgList, RooExponential, RooAddPdf, RooProdPdf, RooAbsReal
from array import array
import math

from root_pandas import to_root, read_root
import scipy.signal as sps
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize
from sklearn.neighbors.kde import KernelDensity
from root_numpy import array2root, root2array
import rootplot.root2matplotlib as r2m

#Mode (3pi)
sub_mode = sys.argv[1]

#Geometry (LHCb / all)
geom = sys.argv[2]

vars = ['costheta_D_reco',
		'costheta_L_reco',
		'chi_reco',
		'costheta_D_true',
		'costheta_L_true',
		'chi_true'
		]

tree = TChain("DecayTree")
tree.Add('/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples/Bd2DstTauNu/%s_%s_Total/model_vars.root' % (sub_mode,geom))
tree.SetBranchStatus("*",0)

for v in vars:
	tree.SetBranchStatus(v,1)

angles = ['costheta_D','costheta_L','chi']

angle_min = {'costheta_D': -1.,
			 'costheta_L': -1.,
			 'chi': -math.pi,
			 }
			 
angle_max = {'costheta_D': 1.,
			 'costheta_L': 1.,
			 'chi': math.pi,
			 }
			 
title = {'costheta_D': "$cos(\\theta_{D})$",
			 'costheta_L': "$cos(\\theta_{L})$",
			 'chi': "$\\chi$ [rad]",
			 }
			 
#Define phase space for q2
phsp_x = OneDimPhaseSpace("Phsp_x", angle_min['costheta_D'],angle_max['costheta_D'])
#Define phase space for slow pion energy
phsp_y = OneDimPhaseSpace("Phsp_y", angle_min['costheta_L'],angle_max['costheta_L'])
#Define phase space for double charm BDT
phsp_z = OneDimPhaseSpace("Phsp_z", angle_min['chi'],angle_max['chi'])

#Combined 3D angular phase space
phsp = CombinedPhaseSpace("Phsp3D",phsp_x,phsp_y,phsp_z)

#Kernel width (in terms of number of bins)
k = 2.
n_bins = 30

#True PDF based on unbinned fit result i.e. the model used in the generation
rate = 1.0
I1s = 0.395505
I2c = -0.163522
I2s = 0.067485
I3 = -0.113973
I4 = -0.140259
I5 = 0.278063
I6c = 0.325495
I6s = -0.251728
I1c = (4.*rate - 6.*I1s + I2c + 2.*I2s)/3.

pdf_str = "%.5f * x^2" % I1c
pdf_str += "+ %.5f * sin(acos(x))^2" % I1s
pdf_str += "+ %.5f * x^2 * (2.0 * y^2 - 1.0)" % I2c
pdf_str += "+ %.5f * sin(acos(x))^2 * (2.0 * y^2 - 1.0)" % I2s
pdf_str += "+ %.5f * (2.0 * cos(z)^2 - 1.0) * sin(acos(y))^2 * sin(acos(x))^2" % I3
pdf_str += "+ %.5f * cos(z) * (2.0 * sin(acos(y)) * y) * (2.0 * sin(acos(x)) * x)" % I4
pdf_str += "+ %.5f * cos(z) * sin(acos(y)) * (2.0 * sin(acos(x)) * x)" % I5
pdf_str += "+ %.5f * x^2 * y" % I6c
pdf_str += "+ %.5f * sin(acos(x))^2 * y" % I6s

truepdf = FormulaDensity("TruePDF", phsp, pdf_str)

approx = 0
if(sub_mode=="3pi_flat"):
	approx = 0
else:
	approx = truepdf

kde = BinnedKernelDensity("KernelPDF", 
                        phsp,   # Phase space
                        tree, # Input ntuple
                        angles[0]+"_true", angles[1]+"_true", angles[2]+"_true",     # Variable to use
                        #weight_var=None, #Weight variable
                        int(n_bins), int(n_bins), int(n_bins),
                        float(k/n_bins)*float(angle_max['costheta_D']-angle_min['costheta_D']), float(k/n_bins)*float(angle_max['costheta_L']-angle_min['costheta_L']), float(k/n_bins)*float(angle_max['chi']-angle_min['chi']),
                        approx,       # Approximation PDF (0 sfor flat approximation)
                        10000000   # Sample size for MC convolution (0 for binned convolution)
                       )

'''
akde = BinnedKernelDensity("AdaptivePDF", 
                        phsp,   # Phase space
                        tree, # Input ntuple
                        angles[0]+"_true", angles[1]+"_true", angles[2]+"_true",     # Variable to use
                        #weight_var=None, #Weight variable
                        int(n_bins), int(n_bins), int(n_bins),    # Number of bins
                        float(k/n_bins)*float(angle_max['costheta_D']-angle_min['costheta_D']), float(k/n_bins)*float(angle_max['costheta_L']-angle_min['costheta_L']), float(k/n_bins)*float(angle_max['chi']-angle_min['chi']),     # Kernel width (twice bin width)
                        kde, #The KDE above
                        0,       # Approximation PDF (0 sfor flat approximation)
                        10000000   # Sample size for MC convolution (0 for binned convolution)
                       )
'''

#Fill ROOT histogram with the values of the full physics model from akde
hist_full = TH3F("hist_full","",n_bins,angle_min['costheta_D'],angle_max['costheta_D']+1e-10,
							  n_bins,angle_min['costheta_L'],angle_max['costheta_L']+1e-10,
							  n_bins,angle_min['chi'],angle_max['chi']+1e-10)

# Fill histogram with the result of kernel density estimation
kde.project(hist_full)
hist_full.Sumw2()

#3D grid over physical angular range 
xx_phys, yy_phys, zz_phys = np.mgrid[angle_min['costheta_D']:angle_max['costheta_D']:30j,
								     angle_min['costheta_L']:angle_max['costheta_L']:30j,
								     angle_min['chi']:angle_max['chi']:30j]

#Dictionary of functions for each angular term
funcs = {"I1c": xx_phys * xx_phys + 0.*yy_phys + 0.*zz_phys,
	 "I1s": np.sin(np.arccos(xx_phys)) * np.sin(np.arccos(xx_phys)) + 0.*yy_phys + 0.*zz_phys, 
	 "I2c": xx_phys * xx_phys * (2.0 * yy_phys * yy_phys - 1.0) + 0.*zz_phys,
	 "I2s": np.sin(np.arccos(xx_phys)) * np.sin(np.arccos(xx_phys)) * (2.0 * yy_phys * yy_phys - 1.0) + 0.*zz_phys,
	 "I3": np.sin(np.arccos(xx_phys)) * np.sin(np.arccos(xx_phys)) * np.sin(np.arccos(yy_phys)) * np.sin(np.arccos(yy_phys)) * (2.0 * np.cos(zz_phys) * np.cos(zz_phys) - 1.0),
	 "I4": (2.0 * np.sin(np.arccos(xx_phys)) * xx_phys) * (2.0 * np.sin(np.arccos(yy_phys)) * yy_phys) * np.cos(zz_phys),
	 "I5": (2.0 * np.sin(np.arccos(xx_phys)) * xx_phys) * np.sin(np.arccos(yy_phys)) * np.cos(zz_phys),
	 "I6c": xx_phys * xx_phys * yy_phys + 0.*zz_phys,
	 "I6s": np.sin(np.arccos(xx_phys)) * np.sin(np.arccos(xx_phys)) * yy_phys + 0.*zz_phys,
	 "I7": (2.0 * np.sin(np.arccos(xx_phys)) * xx_phys) * np.sin(np.arccos(yy_phys)) * np.sin(zz_phys),
	 "I8": (2.0 * np.sin(np.arccos(xx_phys)) * xx_phys) * (2.0 * np.sin(np.arccos(yy_phys)) * yy_phys) * np.sin(zz_phys),
	 "I9": np.sin(np.arccos(xx_phys)) * np.sin(np.arccos(xx_phys)) * np.sin(np.arccos(yy_phys)) * np.sin(np.arccos(yy_phys)) * (2.0 * np.sin(zz_phys) * np.cos(zz_phys))
	 }

#Histogram containing the true angular distribution for each term
hist_true = {}
	
#Histogram containing the ratio of single term / full model
hist_ratio = {}


coeffs = ["I1c","I1s","I2c","I2s","I3","I4","I5","I6c","I6s","I7","I8","I9"]

coeff_title = {"I1c": "$I_{1c}$",
			   "I1s": "$I_{1s}$",
			   "I2c": "$I_{2c}$",
			   "I2s": "$I_{2s}$",
			   "I3": "$I_{3}$",
			   "I4": "$I_{4}$",
			   "I5": "$I_{5}$",
			   "I6c": "$I_{6c}$",
			   "I6s": "$I_{6s}$",
			   "I7": "$I_{7}$",
			   "I8": "$I_{8}$",
			   "I9": "$I_{9}$"}

#Loop over angular terms
for c in coeffs:
	print "Running over coefficient %s" % c
	
	#Tiny bit extra on upper bin edge to catch everything
	hist_true[c] = TH3F("hist_true_%s" % c,"",n_bins,angle_min['costheta_D'],angle_max['costheta_D']+1e-10,
							    n_bins,angle_min['costheta_L'],angle_max['costheta_L']+1e-10,
							    n_bins,angle_min['chi'],angle_max['chi']+1e-10)
							    
	#Loop over theta_X
	for i in range(0,n_bins):
		#Loop over theta_L
		for j in range(0,n_bins):
			#Loop over chi
			for k in range(0,n_bins):
			
				#theta_X location
				x_val = xx_phys[i,0,0]
				#theta_L location
				y_val = yy_phys[0,j,0]
				#chi location
				z_val = zz_phys[0,0,k]
			
				hist_x_true = hist_true[c].GetXaxis()
				bin_x_true = hist_x_true.FindBin(x_val)

				hist_y_true = hist_true[c].GetYaxis()
				bin_y_true = hist_y_true.FindBin(y_val)
	
				hist_z_true = hist_true[c].GetZaxis()
				bin_z_true = hist_z_true.FindBin(z_val)
			
				#Fill angular histogram with the single angular function value
				hist_true[c].SetBinContent(bin_x_true,bin_y_true,bin_z_true,funcs[c][i,j,k])


#Make ratio histograms from hist_true / hist_full ratio

for c in coeffs:
	hist_true[c].Sumw2()
	hist_ratio[c] = hist_true[c].Clone("hist_ratio_%s" % c)
	hist_ratio[c].Divide(hist_full)

#Write the histograms to file for use in fit
out_file = TFile("weight_hists_%s_%s.root" % (sub_mode,geom),"RECREATE")
out_file.cd()
hist_full.Write()
for c in coeffs:
	hist_true[c].Write()
	hist_ratio[c].Write()
out_file.Write()
out_file.Close()