#!/bin/bash

cd /home/dhill/bd2dsttaunu_angular/Make_Angular_Templates

for mode in 3pi 3pipi0
do
    for geom in LHCb all
    do
	 echo "Submitting ${mode} ${geom}"
	    qsub -l cput=3:59:59 -lnodes=1:ppn=2 -v Mode="${mode}",Geom="${geom}" run.sh
    done
done