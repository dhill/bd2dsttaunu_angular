from ROOT import TH3F, TH2F, TFile, TChain
from root_pandas import to_root, read_root
import scipy.signal as sps
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize
from sklearn.neighbors.kde import KernelDensity
import math
from root_numpy import array2root, root2array
import rootplot.root2matplotlib as r2m
from array import array
import sys,os

mode = "Bd2DstTauNu"

#3pi, 3pipi0 
sub_mode = sys.argv[1]

#all or LHCb geometry
geom = sys.argv[2]


in_file = '/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples/%s/%s_%s_Total/model_vars' % (mode,sub_mode,geom)
out_file = in_file+"_weights"

tree = TChain("DecayTree")
tree.Add(in_file+".root")

#Set addresses of true angles, used to find which weight bin the event falls in
costheta_D_true = array( 'd', [0] )
tree.SetBranchAddress( "costheta_D_true", costheta_D_true )

costheta_L_true = array( 'd', [0] )
tree.SetBranchAddress( "costheta_L_true", costheta_L_true )

chi_true = array( 'd', [0] )
tree.SetBranchAddress( "chi_true", chi_true )

new_file = TFile(out_file+".root","RECREATE")
new_tree = tree.CloneTree(0)

#New weight branches for each angular term
coeffs = ["I1c","I1s","I2c","I2s","I3","I4","I5","I6c","I6s","I7","I8","I9"]

w = {}

for c in coeffs:
	w[c] = array( 'd', [0] )
	new_tree.Branch("w_%s" % c, w[c], "w_%s/D" % c)

#Weight histograms
hist_file = TFile.Open("/home/$USER/bd2dsttaunu_angular/Make_Angular_Templates/weight_hists_3pi_%s.root" % geom)

h = {}

for c in coeffs:
	h[c] = hist_file.Get("hist_ratio_%s" % c)


#Loop over tree, anf fill weights based on which bin the events fall into
for i in range(0,tree.GetEntries()):
	
	tree.GetEntry(i)
	
	#Loop over the coefficients
	for c in coeffs:
		
		hist_x = h[c].GetXaxis()
		bin_x = hist_x.FindBin(costheta_D_true[0])
		
		hist_y = h[c].GetYaxis()
		bin_y = hist_y.FindBin(costheta_L_true[0])
		
		hist_z = h[c].GetZaxis()
		bin_z = hist_z.FindBin(chi_true[0])
		
		#Assign weight to event as the content of the histogram bin
		w[c][0] = h[c].GetBinContent(bin_x,bin_y,bin_z)
	
	new_tree.Fill()

new_file.cd()
new_tree.AutoSave()
new_file.Write()
new_file.Close()
