from ROOT import TH3F, TH2F, TFile, TChain
from root_pandas import to_root, read_root
import pandas as pd
import scipy.signal as sps
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize
from sklearn.neighbors.kde import KernelDensity
import math
from root_numpy import array2root, root2array
import rootplot.root2matplotlib as r2m
from array import array
from tqdm import tqdm
import sys,os

mode = "Bd2DstTauNu"
sub_mode = "3pi_flat"

#all or LHCb geometry
geom = sys.argv[1]

path = "/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples"

full_path = "%s/%s/%s_%s_Total" % (path,mode,sub_mode,geom)

in_file = "%s/model_vars" % full_path

out_file = "%s_weights" % in_file

if os.path.exists(out_file+".root"):
	os.remove(out_file+".root")

vars = ["costheta_D_true",
		"costheta_D_reco",
		"costheta_L_true",
		"costheta_L_reco",
		"chi_true",
		"chi_reco",
		"q2_true",
		"q2_reco"]

for df in tqdm(read_root(in_file+".root","DecayTree",chunksize=100000,columns=vars)):
	
	x = df["costheta_D_true"]
	y = df["costheta_L_true"]
	z = df["chi_true"]
	
	#Calculate each angular coefficient
	df["w_I1c"] = x * x
	df["w_I1s"] = np.sin(np.arccos(x)) * np.sin(np.arccos(x))
	df["w_I2c"] = x * x * (2.0 * y * y - 1.0)
	df["w_I2s"] = np.sin(np.arccos(x)) * np.sin(np.arccos(x)) * (2.0 * y * y - 1.0)
	df["w_I3"] = np.sin(np.arccos(x)) * np.sin(np.arccos(x)) * np.sin(np.arccos(y)) * np.sin(np.arccos(y)) * (2.0 * np.cos(z) * np.cos(z) - 1.0)
	df["w_I4"] = (2.0 * np.sin(np.arccos(x)) * x) * (2.0 * np.sin(np.arccos(y)) * y) * np.cos(z)
	df["w_I5"] = (2.0 * np.sin(np.arccos(x)) * x) * np.sin(np.arccos(y)) * np.cos(z)
	df["w_I6c"] = x * x * y
	df["w_I6s"] = np.sin(np.arccos(x)) * np.sin(np.arccos(x)) * y
	df["w_I7"] = (2.0 * np.sin(np.arccos(x)) * x) * np.sin(np.arccos(y)) * np.sin(z)
	df["w_I8"] = (2.0 * np.sin(np.arccos(x)) * x) * (2.0 * np.sin(np.arccos(y)) * y) * np.sin(z)
	df["w_I9"] = np.sin(np.arccos(x)) * np.sin(np.arccos(x)) * np.sin(np.arccos(y)) * np.sin(np.arccos(y)) * (2.0 * np.sin(z) * np.cos(z))
	
	#Make all columns of numeric type
	df.apply(pd.to_numeric)

	#Append this dataframe chunk to the output file with the 'a' append mode
	df.to_root(out_file+".root", key='DecayTree',mode='a')
