import os, sys
os.environ["ROOT_INCLUDE_PATH"] = os.pathsep + "/home/dhill/Meerkat/inc/"

from ROOT import gSystem, gStyle, RooRealVar, gApplication, TChain, AddressOf

gSystem.Load("/home/dhill/Meerkat/lib/libMeerkat.so")

from ROOT import OneDimPhaseSpace, FormulaDensity, CombinedPhaseSpace
from ROOT import Logger
from ROOT import BinnedKernelDensity, BinnedDensity, AdaptiveKernelDensity
from ROOT import TFile, TNtuple, TCanvas, TH1F, TH2F, TH3F, TText, vector
from ROOT import RooFit, RooDataSet, RooDataHist, RooHistPdf, RooRealVar, RooFormulaVar, RooArgSet, RooArgList, RooExponential, RooAddPdf, RooProdPdf, RooAbsReal
from array import array
import math

from root_pandas import to_root, read_root
import scipy.signal as sps
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize
from astropy.convolution import convolve_fft, CustomKernel
from sklearn.neighbors.kde import KernelDensity
from root_numpy import array2root, root2array
import rootplot.root2matplotlib as r2m

#Create weighted templates for each angular coefficient

tree = TChain("DecayTree")
tree.Add("/data/lhcb/users/hill/DstTauNu/tuples/RapidSim/Bd2DstTauNu_Tau2PiPiPiNu/ForAcceptance/model_tree_vars_thetaL_pred_angle_weights.root")
tree.SetBranchStatus("*",0)

vars = ['costheta_X_max_max_reco',
		'costheta_L_max_max_reco',
		'costheta_L_pred',
		'chi_max_max_reco',
		'costheta_X_true',
		'costheta_L_true',
		'chi_true',
		'w_I1c',
		'w_I1s',
		'w_I2c',
		'w_I2s',
		'w_I3',
		'w_I4',
		'w_I5',
		'w_I6c',
		'w_I6s',
		'w_I7',
		'w_I8',
		'w_I9'
		]

for v in vars:
	tree.SetBranchStatus(v,1)


angles = ['costheta_X','costheta_L','chi']

angle_min = {'costheta_X': -1.,
			 'costheta_L': -1.,
			 'chi': -math.pi,
			 }
			 
angle_max = {'costheta_X': 1.,
			 'costheta_L': 1.,
			 'chi': math.pi,
			 }

#Loop over coefficients and create templates
coeffs = ["I1c","I1s","I2c","I2s","I3","I4","I5","I6c","I6s","I7","I8","I9"]

#Make true and reco templates
type = ["true","max_max_reco"]

#Dictionary of histograms
h = {}

for c in coeffs:

	for t in type:
	
		print "Making %s %s template" % (c,t)
		
		n_bins = 15
		
		#Fill ROOT histogram with the values of the full physics model from akde
		h["%s_%s" % (c,t)] = TH3F("hist_%s_%s" % (c,t),"",n_bins,angle_min['costheta_X'],angle_max['costheta_X'],
							  n_bins,angle_min['costheta_L'],angle_max['costheta_L'],
							  n_bins,angle_min['chi'],angle_max['chi'])
		
		if(t=="max_max_reco"):
			thetaL_type = "pred"
		else:
			thetaL_type = "true"
		tree.Draw("%s_%s:%s_%s:%s_%s>>hist_%s_%s" % (angles[2],t,angles[1],thetaL_type,angles[0],t,c,t),"w_%s" % c,"goff")
		
		#Normalise the histogram
		#h["%s_%s" % (c,t)].Scale(1.0/h["%s_%s" % (c,t)].GetEntries())

#Write histograms to file
out_file = TFile("template_hists.root","RECREATE")
out_file.cd()
for c in coeffs:
	for t in type:
		h["%s_%s" % (c,t)].Write()

out_file.Write()
out_file.Close()

