from ROOT import TH3F, TH2F, TFile, TChain
from root_pandas import to_root, read_root
import scipy.signal as sps
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize
from sklearn.neighbors.kde import KernelDensity
import math
from root_numpy import array2root, root2array
import rootplot.root2matplotlib as r2m
from array import array
import sys,os

#3pi or 3pipi0
sub_decay = sys.argv[1]

#all or LHCb
geom = sys.argv[2]

from matplotlib import rc 
rc('font',**{'family':'serif','serif':['Roman']}) 
rc('text', usetex=True)

vars = ["costheta_D_true",
		"costheta_D_reco",
		"costheta_L_true",
		"costheta_L_reco",
		"chi_true",
		"chi_reco"
		]
		
weights = ["w_I1c",
		"w_I1s",
		"w_I2c",
		"w_I2s",
		"w_I3",
		"w_I4",
		"w_I5",
		"w_I6c",
		"w_I6s",
		"w_I7",
		"w_I8",
		"w_I9"
		]

all_vars = vars+weights

w_name = {"w_I1c": "$I_{1c}$",
		"w_I1s": "$I_{1s}$",
		"w_I2c": "$I_{2c}$",
		"w_I2s": "$I_{2s}$",
		"w_I3": "$I_{3}$",
		"w_I4": "$I_{4}$",
		"w_I5": "$I_{5}$",
		"w_I6c": "$I_{6c}$",
		"w_I6s": "$I_{6s}$",
		"w_I7": "$I_{7}$",
		"w_I8": "$I_{8}$",
		"w_I9": "$I_{9}$"
		}

all_vars = vars + weights

df = read_root("/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples/Bd2DstTauNu/%s_%s_Total/model_vars_weights.root" % (sub_decay,geom),columns=all_vars)

angles = ['costheta_D','costheta_L','chi']

angle_min = {'costheta_D': -1.,
			 'costheta_L': -1.,
			 'chi': -math.pi,
			 }
			 
angle_max = {'costheta_D': 1.,
			 'costheta_L': 1.,
			 'chi': math.pi,
			 }

title = {'costheta_D': "$\\cos(\\theta_{D})$",
			 'costheta_L': "$\\cos(\\theta_{L})$",
			 'chi': "$\\chi$ [rad]",
			 }

#Default plot with the full model shown
for a in angles:

		fig, ax = plt.subplots(figsize=(7,7))
	
		plt.hist(df['%s_true' % a],bins=25,range=(angle_min[a],angle_max[a]),histtype='step',label='True',color='Blue')
		plt.hist(df['%s_reco' % a],bins=25,range=(angle_min[a],angle_max[a]),histtype='step',label='Reco.',color='Red')
	
		plt.xlabel('%s' % title[a],fontsize=20)
	
		plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
          	ncol=2, fancybox=False, shadow=False)
	
		#plt.show()
		fig.savefig('figs/Full_Model_%s_%s_%s.pdf' % (a,sub_decay,geom))


#Make 1D projections of the true and reco angles with angular coefficient weights applied
for w in weights:
	
	for a in angles:
	
		fig, ax = plt.subplots(figsize=(7,7))
	
		plt.hist(df['%s_true' % a],weights=df[w],bins=25,range=(angle_min[a],angle_max[a]),histtype='step',label='True',color='Blue')
		plt.hist(df['%s_reco' % a],weights=df[w],bins=25,range=(angle_min[a],angle_max[a]),histtype='step',label='Reco.',color='Red')
	
		plt.xlabel('%s, %s' % (title[a],w_name[w]),fontsize=20)
	
		plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
          	ncol=2, fancybox=False, shadow=False)
	
		#plt.show()
		fig.savefig('figs/%s_%s_%s_%s.pdf' % (w,a,sub_decay,geom))


#2D plots of the default model
for a1 in angles:
	for a2 in angles:
		
		if(a1!=a2):
			
			#True
			fig, ax = plt.subplots(figsize=(7,7))
			
			h = ax.hist2d(df['%s_true' % a1],df['%s_true' % a2],bins=25,range=[[angle_min[a1],angle_max[a1]], [angle_min[a2],angle_max[a2]]])
			
			plt.xlabel('True %s' % title[a1],fontsize=20)
			plt.ylabel('True %s' % title[a2],fontsize=20)
			
			plt.colorbar(h[3], ax=ax)
			plt.tight_layout()
			
			#plt.show()
			fig.savefig('figs/Full_Model_True_%s_vs_%s_%s_%s.pdf' % (a1,a2,sub_decay,geom))
			
			#Reco
			fig, ax = plt.subplots(figsize=(7,7))
	
			h = ax.hist2d(df['%s_reco' % a1],df['%s_reco' % a2],bins=25,range=[[angle_min[a1],angle_max[a1]], [angle_min[a2],angle_max[a2]]])
			
			plt.xlabel('Reco %s' % title[a1],fontsize=20)
			plt.ylabel('Reco %s' % title[a2],fontsize=20)
			
			plt.colorbar(h[3], ax=ax)
			plt.tight_layout()
			
			#plt.show()
			fig.savefig('figs/Full_Model_Reco_%s_vs_%s_%s_%s.pdf' % (a1,a2,sub_decay,geom))


#2D plots for each angular term
for w in weights:
	
	for a1 in angles:
		for a2 in angles:
		
			if(a1!=a2):
			
				#True
				fig, ax = plt.subplots(figsize=(7,7))
			
				h = ax.hist2d(df['%s_true' % a1],df['%s_true' % a2],weights=df[w],bins=25,range=[[angle_min[a1],angle_max[a1]], [angle_min[a2],angle_max[a2]]])
			
				plt.xlabel('True %s' % title[a1],fontsize=20)
				plt.ylabel('True %s' % title[a2],fontsize=20)
			
				plt.colorbar(h[3], ax=ax)
				plt.tight_layout()
				
				#plt.show()
				fig.savefig('figs/%s_True_%s_vs_%s_%s_%s.pdf' % (w,a1,a2,sub_decay,geom))
			
				#Reco
				fig, ax = plt.subplots(figsize=(7,7))
				
				h = ax.hist2d(df['%s_reco' % a1],df['%s_reco' % a2],weights=df[w],bins=25,range=[[angle_min[a1],angle_max[a1]], [angle_min[a2],angle_max[a2]]])
			
				plt.xlabel('Reco %s' % title[a1],fontsize=20)
				plt.ylabel('Reco %s' % title[a2],fontsize=20)
			
				plt.colorbar(h[3], ax=ax)
				plt.tight_layout()
			
				#plt.show()
				fig.savefig('figs/%s_Reco_%s_vs_%s_%s_%s.pdf' % (w,a1,a2,sub_decay,geom))
