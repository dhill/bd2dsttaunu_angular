#!/bin/bash

cd /home/dhill/bd2dsttaunu_angular/

source Python2Env/bin/activate

cd Meerkat_local

cp BinnedKernelDensity.cpp ../Meerkat/src/
cp AbsDensity.cpp ../Meerkat/src/
cp AbsDensity.hh ../Meerkat/inc/
cd ../Meerkat
make