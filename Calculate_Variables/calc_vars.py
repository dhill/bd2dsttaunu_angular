import os, sys
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
from root_pandas import to_root, read_root
import hepvector
from hepvector import LorentzVector
from tqdm import tqdm
import random

def calc_mXY(x, y):

    v_X = LorentzVector(df["%s_PX" % x].values,
                        df["%s_PY" % x].values,
                        df["%s_PZ" % x].values,
                        df["%s_E" % x].values)

    v_Y = LorentzVector(df["%s_PX" % y].values,
                        df["%s_PY" % y].values,
                        df["%s_PZ" % y].values,
                        df["%s_E" % y].values)

    v_comb = v_X + v_Y

    m = v_comb.mag

    return m

#Calculate 4-vector of the 3pi system
def calc_3pi():

	v_Pi = []
	
	for i in range(0,3):
		
		v_Pi.append(LorentzVector(df["Tau_Pi%s_PX" % (i+1)].values,
								  df["Tau_Pi%s_PY" % (i+1)].values,
								  df["Tau_Pi%s_PZ" % (i+1)].values,
								  df["Tau_Pi%s_E" % (i+1)].values))
	
	v_3pi = v_Pi[0] + v_Pi[1] + v_Pi[2]
	
	return v_3pi

#Calculate decay q2
def calc_q2(type):
	
	if(type=="true"):
		tau_suf = "_TRUE"
		dst_suf = "_TRUE"
	else:
		tau_suf = "_reco"
		dst_suf = ""
	
	#Array of Lorentz vectors
	v_B = LorentzVector(df["B0_PX%s" % tau_suf].values,
						df["B0_PY%s" % tau_suf].values,
						df["B0_PZ%s" % tau_suf].values,
						df["B0_E%s" % tau_suf].values)
	
	v_Dst = LorentzVector(df["Dst_PX%s" % dst_suf].values,
						  df["Dst_PY%s" % dst_suf].values,
						  df["Dst_PZ%s" % dst_suf].values,
						  df["Dst_E%s" % dst_suf].values)
	
	v_q = v_B - v_Dst
	
	q2 = v_q.mag2
	
	#Returns array of q2 values
	return q2


def calc_theta_L(type):
	
	if(type=="true"):
		tau_suf = "_TRUE"
		dst_suf = "_TRUE"
		b_suf = "_TRUE"
	else:
		tau_suf = "_reco"
		dst_suf = ""
		b_suf = "_reco"
	
	v_Tau = LorentzVector(df["Tau_PX%s" % tau_suf].values,
						  df["Tau_PY%s" % tau_suf].values,
						  df["Tau_PZ%s" % tau_suf].values,
						  df["Tau_E%s" % tau_suf].values)
	
	v_Dst = LorentzVector(df["Dst_PX%s" % dst_suf].values,
						  df["Dst_PY%s" % dst_suf].values,
						  df["Dst_PZ%s" % dst_suf].values,
						  df["Dst_E%s" % dst_suf].values)
	 
	v_B = LorentzVector(df["B0_PX%s" % b_suf].values,
						df["B0_PY%s" % b_suf].values,
						df["B0_PZ%s" % b_suf].values,
						df["B0_E%s" % b_suf].values)
	
	v_W = v_B - v_Dst
	
	#Boost to B rest frame first
	B_boost = -(v_B.boostp3)
	
	v_Tau.boost(B_boost,inplace=True)
	v_B.boost(B_boost,inplace=True)
	v_W.boost(B_boost,inplace=True)
	
	#Now boost to the W rest frame
	W_boost = -(v_W.boostp3)
	
	#inplace is important!! Wrong results calculated otherwise
	v_Tau.boost(W_boost,inplace=True)
	v_B.boost(W_boost,inplace=True)
	
	vec_Tau = v_Tau.p3.unit
	vec_B = v_B.p3.unit
	
	theta_L = vec_Tau.angle(vec_B)
	
	return theta_L


def calc_theta_D(type):
	
	if(type=="true"):
		d0_suf = "_TRUE"
		dst_suf = "_TRUE"
		b_suf = "_TRUE"
	else:
		d0_suf = ""
		dst_suf = ""
		b_suf = "_reco"
	
	v_D0 = LorentzVector(df["D0_PX%s" % d0_suf].values,
					    df["D0_PY%s" % d0_suf].values,
					    df["D0_PZ%s" % d0_suf].values,
					    df["D0_E%s" % d0_suf].values)
	
	v_Dst = LorentzVector(df["Dst_PX%s" % dst_suf].values,
						df["Dst_PY%s" % dst_suf].values,
						df["Dst_PZ%s" % dst_suf].values,
						df["Dst_E%s" % dst_suf].values)
	
	v_B = LorentzVector(df["B0_PX%s" % b_suf].values,
						df["B0_PY%s" % b_suf].values,
						df["B0_PZ%s" % b_suf].values,
						df["B0_E%s" % b_suf].values)
	
	#Boost to B rest frame first
	B_boost = -(v_B.boostp3)
	
	v_D0.boost(B_boost,inplace=True)
	v_Dst.boost(B_boost,inplace=True)
	v_B.boost(B_boost,inplace=True)
	
	#Now boost to X frame
	Dst_boost = -(v_Dst.boostp3)
	v_D0.boost(Dst_boost,inplace=True)
	v_B.boost(Dst_boost,inplace=True)
		
	vec_D0 = v_D0.p3.unit
	vec_B = v_B.p3.unit
	
	theta_D = vec_D0.angle(vec_B)
	
	return theta_D


def calc_chi(type):
	
	if(type=="true"):
		tau_suf = "_TRUE"
		dst_suf = "_TRUE"
		b_suf = "_TRUE"
		d0_suf = "_TRUE"
	else:
		tau_suf = "_reco"
		dst_suf = ""
		b_suf = "_reco"
		d0_suf = ""
	
	v_Tau = LorentzVector(df["Tau_PX%s" % tau_suf].values,
						  df["Tau_PY%s" % tau_suf].values,
						  df["Tau_PZ%s" % tau_suf].values,
						  df["Tau_E%s" % tau_suf].values)
	
	v_D0 = LorentzVector(df["D0_PX%s" % d0_suf].values,
						 df["D0_PY%s" % d0_suf].values,
						 df["D0_PZ%s" % d0_suf].values,
						 df["D0_E%s" % d0_suf].values)
	
	v_Dst = LorentzVector(df["Dst_PX%s" % dst_suf].values,
						df["Dst_PY%s" % dst_suf].values,
						df["Dst_PZ%s" % dst_suf].values,
						df["Dst_E%s" % dst_suf].values)
	
	v_B = LorentzVector(df["B0_PX%s" % b_suf].values,
						df["B0_PY%s" % b_suf].values,
						df["B0_PZ%s" % b_suf].values,
						df["B0_E%s" % b_suf].values)
		
	#4-vector of neutrino from W decay
	v_W = v_B - v_Dst
	v_Nu = v_W - v_Tau
	
	#Boost everything to the B frame
	B_boost = -(v_B.boostp3)
	
	v_B.boost(B_boost,inplace=True)
	v_W.boost(B_boost,inplace=True)
	v_Tau.boost(B_boost,inplace=True)
	v_Dst.boost(B_boost,inplace=True)
	v_Nu.boost(B_boost,inplace=True)
	v_D0.boost(B_boost,inplace=True)
	
	vec_Dst = v_Dst.p3.unit
	vec_D0 = v_D0.p3.unit
	vec_DstD0 = vec_Dst.cross(vec_D0)
	
	vec_W = v_W.p3.unit
	vec_Tau = v_Tau.p3.unit
	vec_WTau = vec_W.cross(vec_Tau)
	
	vec_x = vec_DstD0.unit
	vec_z = vec_Dst
	vec_y = (vec_z.cross(vec_x)).unit
	
	c = vec_WTau.dot(vec_x)
	s = vec_WTau.dot(vec_y)
	
	chi = np.arctan2(s,c)
	
	return chi


def calc_m2_miss(type):
	
	if(type=="true"):
		dst_suf = "_TRUE"
		b_suf = "_TRUE"
	else:
		dst_suf = ""
		b_suf = "_reco"
	
	v_B = LorentzVector(df["B0_PX%s" % b_suf].values,
						df["B0_PY%s" % b_suf].values,
						df["B0_PZ%s" % b_suf].values,
						df["B0_E%s" % b_suf].values)
	
	v_Dst = LorentzVector(df["Dst_PX%s" % dst_suf].values,
						  df["Dst_PY%s" % dst_suf].values,
						  df["Dst_PZ%s" % dst_suf].values,
						  df["Dst_E%s" % dst_suf].values)
	
	v_3pi = calc_3pi()
		
	#Boost all to B rest frame
	B_boost = -(v_B.boostp3)
	v_Dst.boost(B_boost,inplace=True)
	v_3pi.boost(B_boost,inplace=True)
		
	return pow((M_B - (v_Dst + v_3pi).mag),2)


def calc_tau_life(type):
	
	if(type=="true"):
		tau_suf = "_TRUE"
	else:
		tau_suf = "_reco"

	c_light = 299792458000.
	#Conversion to ps (1e12 / 1000 to take FD from mm to m)
	conv = 1e9
	
	v_Tau = LorentzVector(df["Tau_PX%s" % tau_suf].values,
						  df["Tau_PY%s" % tau_suf].values,
						  df["Tau_PZ%s" % tau_suf].values,
						  df["Tau_E%s" % tau_suf].values)
	
	Tau_gamma = v_Tau.gamma
	Tau_beta = v_Tau.beta
	
	if(type=="reco"):
		suf = ""
	else:
		suf = "_TRUE"
	
	Tau_FD = df["Tau_FD%s" % suf].values
	
	fac = np.multiply(c_light,Tau_beta)
	fac = np.multiply(fac,Tau_gamma)
	lt = np.divide(Tau_FD,fac)
	lt = np.multiply(lt,conv)
	
	return lt


#B decay mode
mode = sys.argv[1]

#Sub-decay mode
sub_mode = sys.argv[2]

#Geometry
geom = sys.argv[3]

path = "/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples"

full_path = "%s/%s/%s_%s_Total" % (path,mode,sub_mode,geom)

in_file = "%s/model" % full_path

out_file = "%s_vars" % in_file

if os.path.exists(out_file+".root"):
	os.remove(out_file+".root")

for df in tqdm(read_root(in_file+".root","DecayTree",chunksize=100000)):
	
	
	#Smearing on the truth-level vertices
	#(reco - true)/true vertex values taken from fits to 2016 full sim MC (mm)
	
	#PV
	B_Ori_x_err = (1.1913e-02*6.2648e-01 + 6.8986e-03*(1.-6.2648e-01))
	B_Ori_y_err = (1.1913e-02*6.2648e-01 + 6.8986e-03*(1.-6.2648e-01))
	B_Ori_z_err = (1.1982e-03*6.2604e-01 + 4.7052e-04*(1.-6.2604e-01))
	
	#B end vertex (and tau birth)
	B_End_x_err = (3.0694e-02*5.8797e-01 + 8.3885e-03*(1.-5.8797e-01))
	B_End_y_err = (4.5619e-03*2.9748e-01 + 3.7015e-02*(1.-2.9748e-01))
	B_End_z_err = (1.4381e-02*5.0443e-01 + 6.2199e-02*(1.-5.0443e-01))
	
	Tau_End_x_err = (4.4770e-03*3.3802e-01 + 2.1096e-02*(1.-3.3802e-01))
	Tau_End_y_err = (1.4991e-02*4.1589e-01 + 5.2997e-02*(1.-4.1589e-01))
	Tau_End_z_err = (8.1314e-03*5.1671e-01 + 3.3336e-02*(1.-5.1671e-01))
	
	df["B0_Ori_x"] = df["B0_Ori_x_TRUE"]*np.random.normal(loc=1.0, scale=B_Ori_x_err, size=len(df))
	df["B0_Ori_y"] = df["B0_Ori_y_TRUE"]*np.random.normal(loc=1.0, scale=B_Ori_y_err, size=len(df))
	df["B0_Ori_z"] = df["B0_Ori_z_TRUE"]*np.random.normal(loc=1.0, scale=B_Ori_z_err, size=len(df))
	
	df["B0_End_x"] = df["B0_End_x_TRUE"]*np.random.normal(loc=1.0, scale=B_End_x_err, size=len(df))
	df["B0_End_y"] = df["B0_End_y_TRUE"]*np.random.normal(loc=1.0, scale=B_End_y_err, size=len(df))
	df["B0_End_z"] = df["B0_End_z_TRUE"]*np.random.normal(loc=1.0, scale=B_End_z_err, size=len(df))
	
	df["Tau_End_x"] = df["Tau_End_x_TRUE"]*np.random.normal(loc=1.0, scale=Tau_End_x_err, size=len(df))
	df["Tau_End_y"] = df["Tau_End_y_TRUE"]*np.random.normal(loc=1.0, scale=Tau_End_y_err, size=len(df))
	df["Tau_End_z"] = df["Tau_End_z_TRUE"]*np.random.normal(loc=1.0, scale=Tau_End_z_err, size=len(df))
	
	#Recalculate flight using these smeared quantities
	df["Tau_FD_x"] = df["Tau_End_x"] - df["B0_End_x"]
	df["Tau_FD_y"] = df["Tau_End_y"] - df["B0_End_y"]
	df["Tau_FD_z"] = df["Tau_End_z"] - df["B0_End_z"]
	df["Tau_FD"] = np.sqrt( df["Tau_FD_x"]**2 + df["Tau_FD_y"]**2 + df["Tau_FD_z"]**2)
	
	df["B0_FD_x"] = df["B0_End_x"] - df["B0_Ori_x"]
	df["B0_FD_y"] = df["B0_End_y"] - df["B0_Ori_y"]
	df["B0_FD_z"] = df["B0_End_z"] - df["B0_Ori_z"]
	df["B0_FD"] = np.sqrt( df["B0_FD_x"]**2 + df["B0_FD_y"]**2 + df["B0_FD_z"]**2)
	
	#PDG tau mass (GeV)
	M_Tau = 1.77682
	
	#List of pions to make the 3pi system from (sometimes have more than 3 in the generated decay, must choose random 3)
	PiP = {}
	PiM = {}
	
	comps = ["PX","PY","PZ","E"]
	
	pip_list = []
	pim_list = []
	
	#At most 3 pi+'s
	for i in range(1,4):
		for c in comps:
			if("PiP_%s_%s" % (i,c) in df.columns):
				PiP["%s_%s" % (i,c)] = df["PiP_%s_%s" % (i,c)]
				if i not in pip_list:
					pip_list.append(i)
	
	#At most 2 pi-'s
	for i in range(1,3):
		for c in comps:
			if("PiM_%s_%s" % (i,c) in df.columns):
				PiM["%s_%s" % (i,c)] = df["PiM_%s_%s" % (i,c)]
				if i not in pim_list:
					pim_list.append(i)

	
	#Randomly shuffle in order to pick random ones
	pip_list_r = random.sample(pip_list,2)
	pim_list_r = random.sample(pim_list,1)
	
	for c in comps:	
		df["Tau_Pi1_%s" % c] = PiP["%s_%s" % (pip_list_r[0],c)]
		df["Tau_Pi2_%s" % c] = PiP["%s_%s" % (pip_list_r[1],c)]
		df["Tau_Pi3_%s" % c] = PiM["%s_%s" % (pim_list_r[0],c)]
	
	#4-vector of the 3pi system 
	v_3pi = calc_3pi()
	df["3pi_M"] = v_3pi.mag.tolist()
	df["3pi_P"] = v_3pi.p.tolist()
	df["3pi_PX"] = v_3pi.p3.x.tolist()
	df["3pi_PY"] = v_3pi.p3.y.tolist()
	df["3pi_PZ"] = v_3pi.p3.z.tolist()
	df["3pi_E"] = v_3pi.e.tolist()
	
	#Invariant mass pairs of the three pions
	df["Tau_m12"] = calc_mXY("Tau_Pi1","Tau_Pi2").tolist()
	df["Tau_m13"] = calc_mXY("Tau_Pi1","Tau_Pi3").tolist()
	df["Tau_m23"] = calc_mXY("Tau_Pi2","Tau_Pi3").tolist()
	
	#Maximum angle between 3pi momentum and tau flight
	df["Tau_angle_max"] = np.arcsin((M_Tau**2 - df["3pi_M"]**2)/(2.*M_Tau*df["3pi_P"]))
	
	#Tau momentum at maximum angle
	df["Tau_P_reco"] = ((df["3pi_M"]**2 + M_Tau**2)*df["3pi_P"]*np.cos(df["Tau_angle_max"]))/(2.*(df["3pi_E"]**2 - df["3pi_P"]**2*(np.cos(df["Tau_angle_max"]))**2))
	
	#Tau momentum components using flight vector
	p_list = ["X","Y","Z"]
	
	for p in p_list:
		
		df["Tau_P%s_reco" % p] = df["Tau_P_reco"]*(df["Tau_FD_%s" % p.lower()]/df["Tau_FD"])
	
	df["Tau_E_reco"] = np.sqrt(df["Tau_P_reco"]**2 + M_Tau**2)
	
	#Tau neutrino (Tau - 3pi)
	
	for p in p_list:
		
		df["Tau_nu_P%s_reco" % p] = df["Tau_P%s_reco" % p] - df["3pi_P%s" % p]

	df["Tau_nu_P_reco"] = df["Tau_P_reco"] - df["3pi_P"]

	df["Tau_nu_E_reco"] = df["Tau_E_reco"] - df["3pi_E"]
	
	#D* + tau quantities
	for p in p_list:
		
		df["DstTau_P%s_reco" % p] = df["Dst_P%s" % p] + df["Tau_P%s_reco" % p]

	df["DstTau_P_reco"] = df["Dst_P"] + df["Tau_P_reco"]

	df["DstTau_E_reco"] = df["Dst_E"] + df["Tau_E_reco"]

	df["DstTau_M_reco"] = np.sqrt(df["DstTau_E_reco"]**2 - df["DstTau_P_reco"]**2)
	
	#PDG tau mass (GeV)
	M_B = 5.27958
	
	df["B0_angle_max"] = np.arcsin((M_B**2 - df["DstTau_M_reco"]**2)/(2*M_B*df["DstTau_P_reco"]))
	
	#B momentum at maximum angle
	df["B0_P_reco"] = ((df["DstTau_M_reco"]**2 + M_B**2)*df["DstTau_P_reco"]*np.cos(df["B0_angle_max"]))/(2*(df["DstTau_E_reco"]**2 - df["DstTau_P_reco"]**2*(np.cos(df["B0_angle_max"]))**2))
	
	#B momentum components 
	for p in p_list:
		
		df["B0_P%s_reco" % p] = df["B0_P_reco"]*(df["B0_FD_%s" % p.lower()]/df["B0_FD"])

	df["B0_E_reco"] = np.sqrt(df["B0_P_reco"]**2 + M_B**2)
	
	#B neutrino (B - DTau)
	for p in p_list:
		
		df["B0_nu_P%s_reco" % p] = df["B0_P%s_reco" % p] - df["DstTau_P%s_reco" % p]

	df["B0_nu_P_reco"] = df["B0_P_reco"] - df["DstTau_P_reco"]

	df["B0_nu_E_reco"] = df["B0_E_reco"] - df["DstTau_E_reco"]
	
	
	type_list = ["reco"]
	
	#Also add truth info if running on a signal decay (used to study resolution)
	if(mode=="Bd2DstTauNu"):
		type_list.append("true")
		
	for t in type_list:
	
		q2 = calc_q2(t)
		df["q2_%s" % t] = q2.tolist()
		
		theta_L = calc_theta_L(t)
		df["theta_L_%s" % t], df["costheta_L_%s" % t] = theta_L.tolist(), (np.cos(theta_L)).tolist()
		
		theta_D = calc_theta_D(t)
		df["theta_D_%s" % t], df["costheta_D_%s" % t] = theta_D.tolist(), (np.cos(theta_D)).tolist()
		
		chi = calc_chi(t)
		df["chi_%s" % t], df["coschi_%s" % t] = chi.tolist(), (np.cos(chi)).tolist()
		
		m2_miss = calc_m2_miss(t)
		df["m2_miss_%s" % t] = m2_miss.tolist()

		Tau_life = calc_tau_life(t)
		df["Tau_life_%s" % t] = Tau_life.tolist()
	
	#Make all columns of numeric type
	df.apply(pd.to_numeric)

	#Append this dataframe chunk to the output file with the 'a' append mode
	df.to_root(out_file+".root", key='DecayTree',mode='a')

print "Output file written"

sys.exit()
