#!/bin/bash

cd /home/dhill/bd2dsttaunu_angular

source Python2Env/bin/activate

cd Fitting/4D_Binned_SignalOnly

for mode in 3pi #3pipi0
do
    for geom in LHCb #all
    do
	for var_type in reco #true
	do
	    for n in 25 50 75 100 200
	    do
		python -b BinnedFit.py ${mode} ${geom} ${var_type} ${n} N
	    done
	done
    done
done