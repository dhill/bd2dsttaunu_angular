import matplotlib.pyplot as plt
import rootplot.root2matplotlib as r2m
import matplotlib.mlab as mlab
import pandas as pd
import sys,os
import numpy as np

#3pi, 3pipi0
sub_mode = sys.argv[1]

#Geometry (LHCb / all)
geom = sys.argv[2]

#True or reco angles
var_type = sys.argv[3]

#Number of events fitted 
num_sig = sys.argv[4]

q2_bins = {"0": (0.0,6.64),
		   "1": (6.64,8.49),
		   "2": (8.49,10.69)
		   }

results = {}
param = {}

for q in q2_bins:
	
	results[q] = pd.read_csv('results/result_%s_%s_%s_%s_q2_%s.txt' % (sub_mode,geom,var_type,num_sig,q),header = None, sep=" ")
	param[q] = pd.read_csv('results/param_%s_%s_%s_%s_q2_%s.txt' % (sub_mode,geom,var_type,num_sig,q),header = None, sep=" ")

#q2 integrated truth-level values from unbinned fit
coeffs = {"I1s" : 0.395679,
		  "I2c" : -0.169499,
		  "I2s" : 0.067240,
		  "I6c" : 0.319545,
		  "I6s" : -0.253748,
		  "I3" : -0.116300,
		  "I4" : -0.141101,
		  "I5" : 0.277062,
		  "I7" : -0.006894,
		  "I8" : 0.002429,
		  "I9" : -0.004141 
		  }

coeffs["I1c"] = (4.0 - 6.0*coeffs["I1s"] + coeffs["I2c"] + 2.0*coeffs["I2s"])/3.0


df = {}

y_vals = {}
y_errs = {}
x_errs = {}

x_vals = {}

for c in coeffs:
	y_vals[c] = []
	y_errs[c] = []
	x_errs[c] = []
	x_vals[c] = []
	for q in q2_bins:
		if(c!="I1c"):
			df["%s_%s" % (c,q)] = results[q][results[q][0].str.contains("%s" % c)]
		else:
			df["%s_%s" % (c,q)] = param[q][param[q][0].str.contains("%s" % c)]
		y_vals[c].append(df["%s_%s" % (c,q)].iat[0,1])
		y_errs[c].append(df["%s_%s" % (c,q)].iat[0,2])
		x_errs[c].append(0.5*(q2_bins[q][1] - q2_bins[q][0]))
		x_vals[c].append(0.5*(q2_bins[q][0] + q2_bins[q][1]))

for c in coeffs:
	
	fig,ax = plt.subplots(figsize=(7,7))
	
	plt.axhline(y=coeffs[c],color='b',alpha=0.4,linestyle='--',label="Truth $q^2$ average")
	
	#Calculate weighted average of the q2 bins
	w_sum = 0
	x_sum = 0
	for i in range(0,len(y_vals[c])):
		w = 1.0/(y_errs[c][i]**2)
		x = w*y_vals[c][i]
		w_sum += w
		x_sum += x
	mean = (1.0/w_sum)*x_sum
	std = 1.0/np.sqrt(w_sum)
	
	plt.xlabel("$q^2$ [GeV$^2/c^4$]",fontsize=15)
	plt.ylabel(c,fontsize=15)
	plt.title("%sk events fit" % num_sig)
	plt.grid(alpha=0.2)
	plt.axhline(y=mean,color='r',alpha=0.4,label="Reco $q^2$ average")
	ax.axhspan(mean-std, mean+std, facecolor='r', alpha=0.2)
	plt.errorbar(x_vals[c],y_vals[c],yerr=y_errs[c],xerr=x_errs[c],fmt='.',capsize=2,color='k',label="Reco results")
	
	plt.ylim(-0.75,0.75)
	plt.legend()
	
	#plt.show()
	
	fig.savefig('figs/%s_vs_q2_%s_%s_%s_%s.pdf' % (c,sub_mode,geom,var_type,num_sig))




  
		

	
