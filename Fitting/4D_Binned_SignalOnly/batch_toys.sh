#!/bin/sh

cd /home/dhill/bd2dsttaunu_angular/Fitting/4D_Binned_SignalOnly/

for toy in {0..999}
do
    echo "Submitting toy ${toy}"
    qsub -l cput=1:59:59 -lnodes=1:ppn=2 run_toy.sh
    sleep 0.1
done