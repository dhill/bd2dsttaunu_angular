import sys,os 
import pandas as pd
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
from scipy import stats
import numpy as np

#3pi or 3pipi0
sub_mode = sys.argv[1]
#Geometry (all or LHCb)
geom = sys.argv[2]
#True or reco angles
var_type = sys.argv[3]
#Number of events to run on (in k) - 5, 10, 20, 40, 80
num_sig = sys.argv[4]

path = 'toys/'
files = [f for f in listdir(path) if isfile(join(path, f))]

#Keep only relevant toys 
result_files = []
param_files = []
for f in files:
	if("%s_%s_%s_%s" % (sub_mode,geom,var_type,num_sig) in f and ".txt" in f and "result" in f):
		result_files.append(f)
	elif("%s_%s_%s_%s" % (sub_mode,geom,var_type,num_sig) in f and ".txt" in f and "param" in f):
		param_files.append(f)

print "Number of toys: %s" % len(result_files)

coeffs = {"I1s": 0.400177,
		  "I2c": -0.152948,
		  "I2s": 0.065706,
		  "I6c": 0.292031,
		  "I6s": -0.241962,
		  "I3": -0.105183,
		  "I4": -0.142294,
		  "I5": 0.279348,
		  "I7": 0.0,
		  "I8": 0.0,
		  "I9": 0.0,
		  "Rate": 0.759755
		  }

coeffs["I1c"] = (4.0 - 6.0*coeffs["I1s"] + coeffs["I2c"] + 2.0*coeffs["I2s"])/3.0

df = {}
vals = {}
errs = {}
pulls = {}

for c in coeffs:
	print "Collecting values for %s" % c
	df[c] = []
	vals[c] = []
	errs[c] = []
	pulls[c] = []
	i = 0
	files = []
	if(c=="I1c"):
		files = param_files
	else:
		files = result_files
	for f in files:
		data = pd.read_csv(path+f, header = None, sep=" ")
		df[c].append(data[data[0].str.contains("%s" % c)])
		vals[c].append(df[c][i].iat[0,1])
		errs[c].append(df[c][i].iat[0,2])
		pulls[c].append((vals[c][i]-coeffs[c])/errs[c][i])
		i += 1
	
	#Plot value distribution along with generated value
	fig,ax = plt.subplots(figsize=(7,7))
	plt.hist(vals[c],bins=15,range=(coeffs[c]-5*np.mean(errs[c]),coeffs[c]+5*np.mean(errs[c])),color='blue',alpha=0.5,label="Toys")
	plt.axvline(x=coeffs[c],linestyle='--',color='r',label="Generated value")
	plt.xlabel("%s" % c)
	#plt.show()
	
	#Plot pull
	fig,ax = plt.subplots(figsize=(7,7))
	
	keep_pulls = []
	for p in pulls[c]:
		if(p > -5 and p < 5):
			keep_pulls.append(p)
		
	plt.hist(keep_pulls,bins=15,range=(-5,5),color='blue',alpha=0.5)
	plt.xlabel("%s pull ($\\sigma$)" % c,fontsize=18)
	mu = np.mean(keep_pulls)
	mu_err = stats.sem(keep_pulls)
	sigma = np.std(keep_pulls)
	plt.axvline(x=0.0,linestyle='--',color='r')
	plt.title("$\\mu = %.3f \\pm %.3f, \\sigma = %.3f$" % (mu,mu_err,sigma))
	#plt.show()
	plt.tight_layout()
	fig.savefig('figs/Pull_Dist_%s_%s_%s_%s_%s.pdf' % (c,sub_mode,geom,var_type,num_sig))
	
		