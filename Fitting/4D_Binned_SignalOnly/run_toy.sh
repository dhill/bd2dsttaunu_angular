#!/bin/bash

cd /home/dhill/bd2dsttaunu_angular

source Python2Env/bin/activate

cd Fitting/4D_Binned_SignalOnly

for mode in 3pi #3pipi0
do
    for geom in LHCb #all
    do
	for var_type in reco #true reco
	do
	    for n in 25 200 #5 10 25 50 75 100 150 200
	    do
		python BinnedFit.py ${mode} ${geom} ${var_type} ${n} Y
	    done
	done
    done
done