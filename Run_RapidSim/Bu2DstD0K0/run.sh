#!/bin/sh

source /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87/gcc/4.9.3/x86_64-slc6/setup.sh
source /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87/ROOT/6.08.02/x86_64-slc6-gcc49-opt/bin/thisroot.sh

cd /data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples
#Top directory
top_dir_name=Bu2DstD0K0
if [ -d $top_dir_name ]
then
    echo "Directory already exists"
else
    mkdir $top_dir_name
fi
cd $top_dir_name

#Batch job subdir
dir_name=${Mode}_${Geom}_${Job_num}
if [ -d $dir_name ]
then
    echo "Directory already exists"
else
    mkdir $dir_name
fi
cd $dir_name

/home/dhill/RapidSim/build/src/RapidSim.exe /home/dhill/bd2dsttaunu_angular/Run_RapidSim/${top_dir_name}/model_${Mode}_${Geom} 100000 1
