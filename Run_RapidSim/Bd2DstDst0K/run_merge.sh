#!/bin/sh

cd /home/dhill/bd2dsttaunu_angular/
source Python2Env/bin/activate

cd /data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples
#Top directory
top_dir_name=Bd2DstDst0K
cd $top_dir_name

#Batch job summed dir
dir_name=${Dst_Mode}_${Mode}_${Geom}_Total
if [ -d $dir_name ]
then
    echo "Directory already exists"
else
    mkdir $dir_name
fi
cd $dir_name

hadd -f model.root ../${Dst_Mode}_${Mode}_${Geom}_*/*_tree.root

#Run variable calculator
python /home/dhill/bd2dsttaunu_angular/Calculate_Variables/calc_vars.py ${top_dir_name} ${Dst_Mode}_${Mode} ${Geom}
