#!/bin/sh

cd /home/dhill/bd2dsttaunu_angular/Run_RapidSim/Bu2DstDK

for mode in ks3pi 3pipi0
do
    for geom in LHCb
    do
	echo "Submitting ${mode} ${geom} ${job_num}"
	qsub -l cput=1:59:59 -lnodes=1:ppn=2 -v Mode="${mode}",Geom="${geom}" run_merge.sh
	sleep 1
    done
done