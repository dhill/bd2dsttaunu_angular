#!/bin/sh

cd /home/dhill/bd2dsttaunu_angular/Run_RapidSim/Bd2DstDs1

for ds_mode in dsstpi0_dsgamma dsgamma
do
    for mode in etapi omegapi etarho omegarho 5pi omega3pi etappi_etapipi etappi_rhogamma etaprho_etapipi etaprho_rhogamma
    do
	for geom in LHCb
	do
	    for job_num in {0..29}
	    do
		echo "Submitting ${mode} ${geom} ${job_num}"
		qsub -l cput=1:59:59 -lnodes=1:ppn=2 -v Ds_Mode="${ds_mode}",Mode="${mode}",Geom="${geom}",Job_num="${job_num}" run.sh
		sleep 0.25
	    done
	done
    done
done