#!/bin/sh

cd /home/dhill/bd2dsttaunu_angular/Run_RapidSim/Bu2DstDst0K0

for dst_mode in d0pi0 d0gamma
do
    for mode in k3pi k3pipi0
    do
	for geom in LHCb
	do
	    echo "Submitting ${mode} ${geom} ${job_num}"
	    qsub -l cput=1:59:59 -lnodes=1:ppn=2 -v Dst_Mode="${dst_mode}",Mode="${mode}",Geom="${geom}" run_merge.sh
	    sleep 1
	done
    done
done