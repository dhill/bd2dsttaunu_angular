#!/bin/sh

cd /home/dhill/bd2dsttaunu_angular/Run_RapidSim/Bd2DstTauNu

for mode in 3pi_flat #3pi 3pipi0
do
    for geom in LHCb #all
    do
	for job_num in {0..99}
	do
	    echo "Submitting ${mode} ${geom} ${job_num}"
	    qsub -l cput=1:59:59 -lnodes=1:ppn=2 -v Mode="${mode}",Geom="${geom}",Job_num="${job_num}" run.sh
	    sleep 1
	done
    done
done