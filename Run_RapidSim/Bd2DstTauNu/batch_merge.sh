#!/bin/sh

cd /home/dhill/bd2dsttaunu_angular/Run_RapidSim/Bd2DstTauNu

for mode in 3pi_flat #3pi 3pipi0
do
    for geom in LHCb #all
    do
	echo "Submitting ${mode} ${geom} ${job_num}"
	qsub -l cput=3:59:59 -lnodes=1:ppn=2 -v Mode="${mode}",Geom="${geom}" run_merge.sh
	sleep 1
    done
done