#!/bin/sh

cd /home/dhill/bd2dsttaunu_angular/Run_RapidSim/Bd2DstD0K

for mode in k3pi k3pipi0
do
    for geom in LHCb
    do
	echo "Submitting ${mode} ${geom} ${job_num}"
	qsub -l cput=1:59:59 -lnodes=1:ppn=2 -v Mode="${mode}",Geom="${geom}" run_merge.sh
	sleep 1
    done
done