# $`B^0 \to D^* \tau \nu`$ angular analysis framework
This repository contains the code for angular analysis studies of the $`B^0 \to D^* \tau \nu`$ decay. Instructions are provided here for how to set up the required Python environment and packages, and how to run various steps of the analysis.

## Setup instructions

### Setting up Python environment
Set up a virtualenv using the `lcg_virtualenv` scripts, specifying the Python 2 version of `LCG_94`:
```bash
cd bd2dsttaunu_angular
git clone https://your_username@gitlab.cern.ch/cburr/lcg_virtualenv.git
./lcg_virtualenv/create_lcg_virtualenv Python2Env LCG_94
```
Activate the virtualenv with:
```bash
source Python2Env/bin/activate
```
LCG makes several packages available, but here we install some additional required packages here:
```bash
pip install hep-ml
pip install root-pandas
pip install rootplot
pip install scikit-hep
pip install tqdm
pip install uncertainties
pip install tensorflow
```
We also need the `hepvector` package which is not available via `pip`. We clone and install it here:
```bash
git clone https://github.com/henryiii/hepvector.git
cd hepvector
python setup.py install
```

### TensorFlowAnalysis
Fitting is performed using the `TensorFlowAnalysis` toolbox developed by Anton Poluektov, which uses `TensorFlow` manipulations interfacing with `Minuit`. Both `TensorFlow` and `ROOT` are available through our Python virtualenv, and we clone a copy of `TensorFlowAnalysis` into our project here:
```bash
cd bd2dsttaunu_angular
git clone https://your_username@gitlab.cern.ch/poluekt/TensorFlowAnalysis.git
```

We make some minor updates to the `Optimisation.py` script in order to set the `Minuit` strategy and return the covariance matrix of the fit. You should do this before running any fitting:

```bash
cp TensorFlowAnalysis_local/Optimisation.py TensorFlowAnalysis/TensorFlowAnalysis/
```

### RapidSim 
`RapidSim` is used to generate large samples of signal and background events, in order to create histogram templates for the angular fit. **In a fresh shell (don't source Python2Env)**, we clone RapidSim and set it up along with `EvtGen`:
```bash
cd bd2dsttaunu_angular
git clone https://github.com/gcowan/RapidSim.git
source /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87/gcc/4.9.3/x86_64-slc6/setup.sh
source /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_87/ROOT/6.08.02/x86_64-slc6-gcc49-opt/bin/thisroot.sh
chmod +x setupEvtGen.sh
./setupEvtGen.sh
export EVTGEN_ROOT=~/bd2dsttaunu_angular/EvtGen/evtgen
mkdir build
cd build
cmake ../RapidSim
make -j4
```

### Meerkat

In the fit, the signal is described using a sum of histogram templates, one for each angular coefficient. The `Meerkat` package is used to calculate weights for use in the template formation. It is downloaded as follows:

```bash
git clone https://your_username@gitlab.cern.ch/poluekt/Meerkat.git
```

We must make some small alterations to the `Meerkat` source code before compiling, in order to read the `Double_t` branches in our nTuple and enable support for 3D histograms in the `BinnedKernelDensity()` function. A shell script will make these changes and compile `Meerkat`:

```bash
cd Meerkat_local/
sh build.sh
```

## Producing RapidSim samples
Sample production is handled by shell scripts which submit batch queue jobs. An example:
```bash
cd bd2dsttaunu_angular/Run_RapidSim/Bd2DstTauNu
sh batch.sh
```
This will submit batch jobs for $`\tau \to 3\pi \nu`$ and $`\tau \to 3\pi\pi^0 \nu`$, where 30 samples of 100,000 events are generated both for LHCb geometry and $`4\pi`$ geometry. Note that the LHCb gemoetry files produced contain fewer than 100,000 events due to the acceptance cut applied. The output files are stored in nTuple format, and can be found in directories like this:
```bash
/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples/Bd2DstTauNu/3pi_all_0/model_3pi_all_tree.root
/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples/Bd2DstTauNu/3pipi0_LHCb_0/model_3pipi0_LHCb_tree.root
```
The single job outputs are merged with the following command, which runs `hadd` on the batch:
```bash
cd bd2dsttaunu_angular/Run_RapidSim/Bd2DstTauNu
sh batch_merge.sh
```
This produces single nTuples in paths such as this:
```bash
/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples/Bd2DstTauNu/3pi_all_Total/model.root
```
A second process is launched immediately after the merging, which adds important variables to the files. This is described below.

### Summary of generated samples
The table below lists all of the $`B`$ decay modes generated in order to create templates for the fit. Samples are grouped into general categories, where each category is decribed by an overall template in the fit.

| Decay | Category | `Run_RapidSim` directory |
| ------ | ------ | ------ |
| $`B^0 \to D^{*-} (\tau^+ \to \pi^+ \pi^+ \pi^- \bar{\nu}_{\tau}) \nu_{\tau}`$ | Signal | `Bd2DstTauNu` |
| $`B^0 \to D^{*-} (\tau^+ \to \pi^+ \pi^+ \pi^- \pi^0 \bar{\nu}_{\tau}) \nu_{\tau}`$ | Signal | `Bd2DstTauNu` |
| $`B^+ \to D_{1}(2420)^0 (\tau^+ \to \pi^+ \pi^+ \pi^- \bar{\nu}_{\tau}) \nu_{\tau}`$ | Feed-down | `Bu2DststTauNu` |
| $`B^+ \to D_{1}(2420)^0 (\tau^+ \to \pi^+ \pi^+ \pi^- \pi^0 \bar{\nu}_{\tau}) \nu_{\tau}`$ | Feed-down | `Bu2DststTauNu` |
| $`B^+ \to D_{2}^*(2460)^0 (\tau^+ \to \pi^+ \pi^+ \pi^- \bar{\nu}_{\tau}) \nu_{\tau}`$ | Feed-down | `Bu2DststTauNu` |
| $`B^+ \to D_{2}^*(2460)^0 (\tau^+ \to \pi^+ \pi^+ \pi^- \pi^0 \bar{\nu}_{\tau}) \nu_{\tau}`$ | Feed-down | `Bu2DststTauNu` |
| $`B^0 \to D^{*-} D_s^+`$ | $`B \to D^* D_s X`$ background | `Bd2DstDs` |
| $`B^0 \to D^{*-} D_s^{*+}`$ | $`B \to D^* D_s X`$ background | `Bd2DstDsst` |
| $`B^0 \to D^{*-} D_{s1}(2460)^+`$ | $`B \to D^* D_s X`$ background | `Bd2DstDs1` |
| $`B^+ \to D_{1}(2420)^0 D_s^+`$ | $`B \to D^* D_s X`$ background | `Bu2DststDs` |
| $`B^+ \to D_{1}(2420)^0 D_s^{*+}`$ | $`B \to D^* D_s X`$ background | `Bu2DststDsst` |
| $`B^+ \to D_{1}(2420)^0 D_{s1}(2460)^+`$ | $`B \to D^* D_s X`$ background | `Bu2DststDs1` |
| $`B^- \to D^{*-} D^0 \bar{K}^0`$ | $`B \to D^* D^0 X`$ background | `Bu2DstD0K0` |
| $`B^- \to D^{*-} D^{*0} \bar{K}^0`$ | $`B \to D^* D^0 X`$ background | `Bu2DstDst0K0` |
| $`B^0 \to D^{*-} D^0 K^+`$ | $`B \to D^* D^0 X`$ background | `Bd2DstD0K` |
| $`B^0 \to D^{*-} D^{*0} K^+`$ | $`B \to D^* D^0 X`$ background | `Bd2DstDst0K` |
| $`B^0 \to D^{*-} D^{*+} \bar{K}^0`$ | $`B \to D^* D^0 X`$ background | `Bd2DstDstK0` |
| $`B^0 \to D^{*-} D^{+} \bar{K}^0`$ | $`B \to D^* D^+ X`$ background | `Bd2DstDK0` |
| $`B^- \to D^{*-} D^{+} K-`$ | $`B \to D^* D^+ X`$ background | `Bu2DstDK` |
| $`B^0 \to D^{*-} \pi^+ \pi^+ \pi^- \pi^0`$ | Prompt background | `B2Dst3piX` |
| $`B^+ \to D^{*-} \pi^+ \pi^+ \pi^+ \pi^-`$ | Prompt background | `B2Dst3piX` |
| $`B^0 \to D^{*-} (\omega \to \pi^+ \pi^- \pi^0) \pi^+`$ | Prompt background | `B2Dst3piX` |
| $`B^0 \to D^{*-} \pi^+ \pi^+ \pi^+ \pi^- \pi^-`$ | Prompt background | `B2Dst3piX` |

The sub-decays of the various charm mesons involved are listed below. The decay names as they appear in the various `.config` files are also shown. Only mesons with more than one possible decay path are explicitly included in the `.config` file name.

The charm decays below all involve the production of three charged pions in addition to other non-reconstucted particles. Together they constitute the largest backgrounds in the fit, and are collectively known as ***double-charm*** modes.

| Charm meson | Decay | Name in `.config` file |
| ------ | ------ | ------ |
| $`D_s^+`$ | $`(\eta \to \pi^+ \pi^- \pi^0) \pi^+`$ | `etapi` |
| | $`(\omega \to \pi^+ \pi^- \pi^0) \pi^+`$ | `omegapi` |
| | $`(\eta \to \pi^+ \pi^- \pi^0) (\rho^+ \to \pi^+ \pi^0)`$ | `etarho` |
| | $`(\omega \to \pi^+ \pi^- \pi^0) (\rho^+ \to \pi^+ \pi^0)`$ | `omegarho` |
| | $`(\rho^0 \to \pi^+ \pi^-) (\rho^0 \to \pi^+ \pi^-) \pi^+`$ | `5pi` |
| | $`(\omega \to \pi^+ \pi^- \pi^0) \pi^+ \pi^+ \pi^-`$ | `omega3pi` |
| | $`(\eta' \to (\eta \to \pi^+ \pi^- \pi^0) \pi^+ \pi^-) \pi^+`$ | `etappi_etapipi` |
| | $`(\eta' \to (\rho^0 \to \pi^+ \pi^-) \gamma) \pi^+`$ | `etappi_rhogamma` |
| | $`(\eta' \to (\eta \to \pi^+ \pi^- \pi^0) \pi^+ \pi^-) (\rho^+ \to \pi^+ \pi^0)`$ | `etaprho_etapipi` |
| | $`(\eta' \to (\rho^0 \to \pi^+ \pi^-) \gamma) (\rho^+ \to \pi^+ \pi^0)`$ | `etaprho_rhogamma` |
| $`D_s^{*+}`$ | $`D_s^+ \gamma`$ | `dsgamma` |
|| $`D_s^+ \pi^0`$ | `dspi0` |
| $`D_{s1}(2460)^+`$ | $`D_s^{*+} \pi^0`$ | `dsstpi0` |
|| $`D_s^+ \gamma`$ | `dsgamma` |
| $`D_{1}(2420)^0`$ | $`D^{*-} \pi^+`$ ||
| $`D_{2}^*(2460)^0`$ | $`D^{*-} \pi^+`$ ||
| $`D^0`$ | $`K^- \pi^+ \pi^+ \pi^-`$ | `K3pi` |
|| $`K^- \pi^+ \pi^+ \pi^- \pi^0`$ | `K3pipi0` |
| $`D^{*0}`$ | $`D^0 \pi^0`$ | `d0pi0` |
|| $`D^0 \gamma`$ | `d0gamma` |
| $`D^+`$ | $`K_s^0 \pi^+ \pi^+ \pi^-`$ | `Ks3pi` |
|| $`\pi^+ \pi^+ \pi^- \pi^0`$ | `3pipi0` |
| $`D^{*+}`$ | $`D^0 \pi^+`$ | |


## Calculation of fit variables

We use the `hepvector` package (`pandas` equivalent of ROOTs `TLorentzVector`) to calculate the truth-level and reconstructed values of $`(q^2,\cos(\theta_D),\cos(\theta_L),\chi)`$. These four variables fully parameterise the decay rate, and in order to calculate them we must estimate the $`B`$ and $`\tau`$ momenta, which are not fully measured due to the presence of two neutrinos in the final state. 

The magnitude of the $`\tau`$ and $`B`$ momenta are estimated using Equations 4-7 from the Run 1 $`R(D^*)`$ [PRD](https://arxiv.org/abs/1708.08856). The momentum components are then calculated using the $`\tau`$ and $`B`$ flight vectors, which are constructed from the three measured decay vertices.

This reconstruction method assumes a maximal opening angle between the $`3\pi`$ momentum direction and the flight direction of the $`\tau`$, which causes biases and resolution effects in each fit variable. The fit strategy, which employs a series of template histograms created from the 4D distribution of reconstructed $`(q^2,\cos(\theta_D),\cos(\theta_L),\chi)`$, explicitly accounts for these effects by building them into the fit PDFs.

The code to calculate the fit variables is here:

```bash
bd2dsttaunu_angular/Calculate_Variables/calc_vars.py
```

As described above, batch jobs are used to create the merged ROOT files. These jobs also run subsequent processes to add the required variables. The resulting files are of the type:

```bash
/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples/Bd2DstTauNu/3pi_all_Total/model_vars.root
```

A summary of the fit variable branch names is provided in the table below. Note that the truth-level information is stored only in the `Bd2DstTauNu` signal nTuples, where it is used to determine the resolution.

| Variable | Reco branch name | Truth-level branch name |
| ------ | ------ | ------ |
| $`q^2`$ [GeV/$`c^2`$] | `q2_reco` | `q2_true` |
| $`\cos(\theta_D)`$ | `costheta_D_reco` | `costheta_D_true` |
| $`\cos(\theta_L)`$ | `costheta_L_reco` | `costheta_L_true` |
| $`\chi`$ [rad] | `chi_reco` | `chi_true` |


## Calculating weights for each angular term

A reweighting procedure is used to create signal templates for each angular coefficient. Histograms containing angular coefficient weights are first constructed for each angular term using `Meerkat`, using the following script:

```bash
cd Make_Angular_Templates
python create_weight_hists.py
```

This script creates 3D PDFs in $`(\cos(\theta_D),\cos(\theta_L),\chi)`$ for each angular coefficient, and divides them by the total model. The total model is given by the truth-level 3D distribution of $`B^0 \to D^* \tau \nu`$ signal candidates as generated in `RapidSim` without any LHCb acceptance cut (this is taken from the `Bd2DstTauNu/3pi_all_Total/model_vars.root` file). An ouput nTuple containing 12 ratio histograms is produced (one for each angular term), where the bin content at each point in phase space is the angular coefficient weight. 

These weight histograms are then used to assign per-event weights to the $`B^0 \to D^* \tau \nu`$ signal events using:

```bash
sh batch.sh
```

This creates new signal nTuples with 12 additional branches, one for each angular coefficient. They can be found for example here:

```bash
/data/lhcb/users/hill/bd2dsttaunu_angular/RapidSim_tuples/Bd2DstTauNu/3pi_all_Total/model_vars_weights.root
```

These weight branches are later used within `TensorFlowAnalysis` to create binned 4D fit templates in $`(q^2,\cos(\theta_D),\cos(\theta_L),\chi)`$ for each angular term. Templates can be constructed using either truth-level or reco-level quantities, since the calculated weights can be applied in general to any set of variables.



